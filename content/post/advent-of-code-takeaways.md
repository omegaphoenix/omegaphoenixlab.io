+++
title = "Advent of Code Takeaways"
date = 2018-02-20T20:14:16-08:00
description = "Lessons from 25 days of frantic midnight programming"
draft = false
ghcommentid = 20
+++

# Thoughts and Takeaways
This was a great challenge and I'm super glad that I lucked into the top 100 the first day which provided extra motivation to keep trying for points.

I'm actually pretty slow at solving tightly scoped problems compared to the rest of the world.  My highest rank was rank 26 on the second part of the 16th day and the most points I scored was on the last challenge which was a good ending.  This exposed a part of my game that I could definitely work on to become a better programmer and software engineer.

These exercises were first recommended to me for learning a new language.  However I ended up coding in Python since I wanted to see how fast I could solve the problems. Being more flexible and using different languages might have helped on a couple days.  For example, there was a day where most of the top solvers were Haskellers.  These questions would be great for learning a new programming language or good practice for a speed run with a more familiar language.

I definitely didn't use the best software engineering practices when I was solving these questions as good variable names and comments didn't necessarily help.  However, this did bite me a couple times when I had to spend time debugging.  The key was to stay clear enough not to introduce any careless bugs into the code.

I had a lot of fun but I probably wouldn't try to do it live next year since it definitely affected my sleep schedule. It takes a while to wind down after the adrenaline rush of racing to solve a programming question. But who knows? By December next year, the hype might overcome my more sensible decisions.

I have definitely been asked at least one of these questions in an interview before and these questions would be good questions for preparing for software engineering interviews since they are pretty tightly scoped and can be solved in a reasonable amount of time.


# Results
```
These are your personal leaderboard statistics. Rank is your position on that leaderboard: 1 means you were the first person to get that star, 2 means the second, 100 means the 100th, etc. Score is the number of points you got for that rank: 100 for 1st, 99 for 2nd, ..., 1 for 100th, and 0 otherwise.

You have 338 points.

      -------Part 1--------   -------Part 2--------
Day       Time  Rank  Score       Time  Rank  Score
 25   00:08:21    41     60   00:08:38    39     62
 24   00:12:05    52     49   00:18:56    73     28
 23   00:09:41   314      0   00:56:51   108      0
 22   00:24:22   280      0   02:11:24   893      0
 21   00:54:54   186      0   00:55:05   161      0
 20   00:40:21   672      0   00:41:15   320      0
 19   02:46:32  1382      0   02:46:58  1336      0
 18   00:26:22   361      0   00:49:21   151      0
 17   01:24:24  1184      0   01:33:06   970      0
 16   00:09:10   117      0   00:14:22    26     75
 15   00:07:37   188      0   00:09:27    96      5
 14   00:15:57   306      0   00:40:03   254      0
 13   12:07:26  4992      0   12:23:54  4021      0
 12   00:10:10   265      0   00:18:33   374      0
 11   11:17:45  4571      0   11:18:13  4314      0
 10   00:14:12   139      0   00:37:35   235      0
  9   00:09:23    98      3   00:10:40    82     19
  8   00:12:25   338      0   00:12:58   292      0
  7   00:17:28   665      0   00:39:53   297      0
  6   00:10:22   225      0   00:12:16   183      0
  5   00:11:37   878      0   00:12:50   707      0
  4   00:03:14   265      0   00:06:50   283      0
  3   00:11:39   162      0   00:39:18   258      0
  2   00:06:21   450      0   00:13:29   393      0
  1   00:03:32    86     15   00:05:32    79     22
```
