+++
title = "Enough. True Measures of Money, Business, and Life"
date = 2018-02-24T14:46:16-08:00
description = "Takeways from John C. Bogle's 2008 book - lessons from the greatest investor of all time and inventor of index funds"
draft = false
tags = ["investing"]
ghcommentid = 21
+++

Here are my notes from reading the greatest book I have read outside of the Bible. This is a poor substitute for reading the entire book, which truly is thought-provoking and valuable but hopefully my summary and thoughts can provide a fraction (perhaps even one basis point) of the value of the contents of [this book](https://amzn.to/2tq2Hka).

# Introduction - How the GOAT mutual fund came to be
John Bogle started work at the Wellington Fund after graduating from Princeton where in his senior thesis, he wrote mutual funds "may make no claim to superiority over the market averages".

During the 1974 bear market, he was fired from his own fund but through thanks to "the stubbornness of an idealist and the soul of a street fighter", he was able to turn the Wellington funds into mutual mutual funds independent of the Wellington Management Company.
Despite this, the newly formed Vanguard did not have the authority to manage their investments or market their fund.
However, the Bogle and his group was not allowed to manage their investments which led to the world's first index fund (known at the time as Bogle's Folly), which need not be "managed".
In response to the constraint on marketing, Vanguard created a "no-load, sales-charge-free marketing system" which led to their impossibly low fees (0.04% for the S\&P500 fund today).

Despite my own claims to be a Boglehead, I would have never imagined that the index funds I own today were thanks to the attempted firing of John Bogle.  In the ultimate "screw you", Bogle created the index fund as we know it today. Despite their idealogical refusal to support affiliate advertising, they have "over $4.5 trillion in assets under management."

John Bogle was 45 when he founded Vanguard which means I have a bit more time to try to contribute as much to society as this titan has.

# Money
There wasn't much new to me in the money section since I had read "A Random Walk Down Wall Street" and all of my Caltech Business, Economics, Management professors had taught me the importance of index funds and the impossibility of beating the market. (One was even a part time professor who started his own investment fund during the 2008 bear market.  But as he often reminded us, his buddy Professor Eugene Fama attributed his returns to "sh!t dumb luck". This professor also just laughed at me when I had the audacity/stupidity to ask for his income tax statements.)

## Too Much Costs, Not Enough Value
The stock market returns a certain percentage each year.  By definition, before fees, actively managed funds will return the same amount as the total market indices.  Complex financial instruments and the fees that result can only hurt investors. Thus the financial sector rips us off in costs while delivering very little additional value. Finally, fee structures such as those of hedge funds pay the managers tons when they make money while the investor losses on the money on the downside.

## Too Much Speculation, Not Enough Investment
The increased turnover the stock market indicates that more and more active managers and speculators are trading in the market.  By definition, "investing is all about the long-term ownership of businesses." And remember, no one can time the market. Bogle refers to Warren Buffett's $1 million bet that a Vanguard US index fund would beat any hedge fund over 10 years - a bet that Buffett won and included in his 2016 Shareholder Letter.

Bogle points out that while there are black swans in the short term, there are no black swans in the long term returns of the US stock market.

My hedge against the potential black swam in the US market (Japan had over 2 decades without rereachiing their stock market peak) has been to hold 30% international funds in my portfolio but perhaps I am just hurting my own returns by overcomplicating.

## Too Much Complexity, Not Enough Simplicity
Bogle points out that at his retirement, even Peter Lynch declared "Most investors would be better off in an index fund." Some innovations such as target retirement fund and life strategy funds have been good but the majority have not been.

Bogle also astutely observes that ETFs, while displayed as neutral by Vanguard's website, could encourage speculation. This is actually a trap I fell into when I first started investing in Vanguard ETFs before moving to their index funds so that I could buy any dollar amount instead of having to buy full shares at a time.

"Fundamental investing" has led to the creation of index funds based on these metrics which are based on past performance.  The worst part is that many times these funds are created right after the 2000 bear market during which they would have outperformed only to underperform in the 2008 financial tumble.
"As the Oracle of Omaha sometimes expresses it, 'There are three i's in every cycle: first the innovator, then the imitator, and finally the idiot.'... Don't be the idiot."

# Business
"Trust is everything, because success depends upon customers' trust in the products they buy, employees' trus tin their leaders, investors' trust in those who invest for them and the public's trust in capitalism....If you do not have integrity, no one will trust you, nor should they." - Bill George, former chief of medical technology pioneer Medtronic
## Too Much Counting, Not Enough Trust
It turns out that our GDP, unemployment rate, and inflation rate has a bunch of hand waving, phantom numbers, and unreasonable assumptions.

Surprisingly, John Bogle even rips Jeremy Siegel's Stocks for the Long Run for it's role in contributing to great bull markets folllowed by one of the worst bear markets by listing a "panoply of information" and attributing the returns to history instead of the sources of stock returns.

The same issue plagues Monte Carlo simulations which rely on historic returns instead othe sources of returns.

Thus the contribution of dividend yields to returns much take into account the actual dividend yield which is 2.3 percent in July 2008 (and 1.78 percent today) versus a historical average of 5 percent.

Our definitions for earnings have changed and "creative accounting" has been encouraged.
Since companies are focused on numbers such as earnings growth, they are pressued to increase them even if they are "subtract[ing] value from you, from me, and from society."

Instead of numbers, people and companies should focus on trust.  Vanguard has impossibly lived up to Bogle's aspirations. Whereas companies such as Betterment and Wealthfront have broken out onto the scene lately, my trust still only goes to Vanguard.  Why?  Because since I've been a customer of Vanguard, they have lowered their low fees further, whereas Betterment recently raised their fees. In addition, Betterment offers affiliate kickbacks which leads to network marketing (which is one step away from a pyramid scheme).

## Too Much Business Conduct, Not Enough Professional Conduct
"Daedalus-the venerable and prestigious journal of the American Academy of Arts and Sciences-...added these wonderful words: 'The primary feature of any profession [is] to serve responsibly, selflessly, and wisely... and to establish [an] inherently ethical relationship between the professional and general society.'"
Bogle mourns that times have changed as these values are no longer kept.  For example the Big Eight in accounting is now the Final Four (or Big Four) and journalism has exposed many scandals.  Even two of the most prominent trial attourneys have been imprisoned on criminal charges. I'm more hopeful that with the age of the internet, it will be an age of accountability rather than loss of values.

Shareholders should be more responsible instead of like sheep. Admittedly in the last vote that Vanguard sent me, I voted exactly how they suggested after reading the ammendments they proposed.  Bogle decrees that CEO compensation should focus on intrinsic value, not stock price and performance of CEOs rather than how much their peer's are being paid.
The standards of business used to be "There are some things that one simply doesn't do." Today if "everyone else is doing it, so I can do it too."

## Too Much Salesmanship, Not Enough Stewardship
Here are Bogle's 5 dreams for the mutual fund industry.

Dream 1 - A Fair Shake for Shareholders

Dream 2 - Serving the Investor for a Lifetime

Dream 3 - Long-Term Investment Horizons

Dream 4 - Serving Long-Term Investors

Dream 5 - Putting Fund Investors in the Driver's Seat

I think he's set a pretty good example with Vanguard and he has achieved all these dreams for his company.

## Too Much Management, Not Enough Leadership
Bogle's 10 rules for building a great organization

1. Make Caring the Soul of the Organization.

2. Forget about Employees.
  Employees come in each day from 9-5, keep his or her mouth shut, and got paid.  A crew member is excited, motivating, committed, and caring.

3. Set High standards and Values - And Stick to Them.

4. Talk the Talk. Repeat the Values Endlessly.

5. Walk the Walk. Actions Speak Louder than Words.

6. Don't Overmanage.
  "How can we possibly measure the qualities of human existence that give our lives and careers meaning?  How about grace kindess, and integrity?  What value do we put on passion, devotion, and trust?"

7. Recognize Individual Achievement.
  The point is to emphasize the value of the individual to the organization as a whole.

8. A Reminder - Loyalty is a Two-Way Street.
  Vanguard shares its earnings with every member of its workforce.

9. Lead and Mange for the Long Term.

10. Press On, Regardless.
  Or as Mad-Eye once put it, "Constant Viligance!"

I am proud to say and humbly reminded that the voyage I am on with Second Spectrum has striking similarities to Bogle's Vanguard voyage.

# Life
## Too Much Focus on Things, Not Enough Focus on Commitment
Whatever you do, do it with boldness and commitment. This applies to work, family, community, faith, and citizenship. "If a job is to be done, best to do it right."

In the short time I've been alive, I've had the pleasure of having to pour my all into projects and competitions, most recently while drinking from the Caltech firehose of knowledge. I wholeheartedly agree with Bogle's sentiment that boldness and commitment lead to providence.
In coming to that realization, I also admit that I am guilty of often not pouring my full boldness and commitment or procrastinating in my commitments.  I have yet to find the balance of full commitment and enough but from now I will try to err on the side of too much.

## Too Many Twenty-First-Century Values, Not Enough Eighteenth-Century Values
Soon we shall know everything that doesn't count, and nothing that does.  The Age of Reason, or the Enlightenment, leaders had strong values.

Ben Franklin's entrepreneurship was one of simply exercising one's energy and ingenuity and the will to conquer - "to succeed for the sake, not of the fruits of success, but of success itself."  At age 30 he founded the Union Fire Company and then a property insurance company, library, academy and college, hospital, and learned society for the benefit of his community.  He invented the lightning rod and the Franklin stove, neither of which he attempted to patent for his own profit.  Contrast this to today's patent wars and enormous executive and hedge fund manager compensation.

Ben Franklin also began each day asking himself "What Good shall I do this day?" He ended the day with "What Good have I done today?" He is a good reminder for myself to continue working on my own character and holding myself to higher standards.

## Too Much "Success," Not Enough Character
In the footnotes of this chapter, Bogle introspects - "Curiously, I don't ever recall thinking much about becoming a success in life, any more than I ever worried much about  failure. I just never forcused on those abstract ideas. Rather, I believed that if I did my best, every day, more than I was asked to do, better than I was expected to do it, my future would take care of itself."

I have been guilty far too often of worrying about goals for success and using the fear of failure to drive myself. Fortunately, now that I have exited the world of academics and traded it for software engineering, I find myself enjoying the process much more and not worrying as much about success or failure.

Bogle accepts that wealth, fame, and power are measures of success. However, wealth is not financial wealthy.  Ill-deserved fame is not the same as fame for real accomplishment. "Power for a worthy purpose [is] what we ought to respect."  Thus, those "who have chosen business carreers continue to ask ourselves whether we're chasing the fake rabbit of success or the real rabbit of meaning, defined by the contriubtions to our society that stem from principle, virtue, and character."

Furthermore, competition should be for the right things.  "[Not] for test scores rather than learning. [Not] for form rather than substance. [Not] for prestige rather than virtue."

Bogle challenges us to be true to ourselves. "Be yourself! And if you're not the kind of person you know you should be--the kind of person you want to be and can be--make yourself a better person."

# Wrapping Up: What's Enough
Bogle finishes off by giving his timeless advice of saving early, often, and regularly so that we can have enough (which is $1 more than we need.).

I aspire to make as big of an impact on the world as John C. Bogle has done.  To do that, I will ask myself each day what good I will do and have done and work with a commitment and loyalty that is so often lacking in the software engineering industry.
