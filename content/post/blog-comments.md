+++
title = "Blog Comments"
date = 2017-12-03T09:40:40-08:00
description = "Adding comments using Github Issues"
draft = false
ghcommentid = 17
+++

Many Hugo themes come with built in Disqus support since Hugo has a built-in Disqus partial. According to Wikipedia, "Disqus is a worldwide blog comment hosting service for web sites and online communities that use a networked platform."  However, adding Disqus to your site also allows them to use third-party ad-tracking which they do to collect user data and monetize it.  We will have no such evil on this site!  Thus, it was time to consider the alternatives.

It seemed like most of the alternatives were either served and kept in third-party databases (like Disqus) or relied on setting up your own database on a server to host comments.  After considering several open source and self hosted systems, I stumbled upon the [HackerNews discussion on using Github Issues](https://news.ycombinator.com/item?id=14170041).

# Tradeoffs
## Disqus and Blog Comment Hosting Services
Disqus started off free and it makes sense that they eventually needed to monetize the data that they collected if were to continue hosting a free blog tier.  However, some users have found that page load times increase from [2 seconds to 6 seconds](http://donw.io/post/github-comments/) with Disqus enabled!  In addition, there a bunch of shady network requests made.

We could look at alternative hosting services but this would put our data at the mercy of other companies.  Even if they aren't collecting data and serving ads to monetize their services yet, they could easily do so in the future.

## Open-source Self-Hosting
Some examples include [Commento](https://github.com/adtac/commento) which is already set up to run in a Docker container and [Isso](https://posativ.org/isso/) which supports Markdown comments, uses a SQLite backend, and is implemented in Python.  However, the [length of the tutorial](https://stiobhart.net/2017-02-24-isso-comments/) convinced me to do some more research before jumping in.

If I were already serving this site on my own server instead of Gitlab Pages, using Isso would be a no-brainer.  The only other drawback would be that I would need to moderate my site were it ever to become popular enough to attract spammers.

## Github Issues
By using Github Issues, I wouldn't need to self-host the comments.  In addition, since this blog's target audience is aspiring software engineers, commenters would be -encouraged- required to sign up for Github.  Although this could be seen as restrictive, Disqus requires you to sign in either with Disqus, Facebook, Google, or Twitter.  Of course, the gold standard may be similar to Isso, which allows anonymous commenting but sites like Quora have been able to cultivate good discussion by requiring your account to be linked to Facebook which usually has your real name.

A major drawback, however, was that users would need to be redirected to the Github Issues page to comment on the post.  Eventually, this may convince me to self host using a comment system like Isso, but until I get complaints about the user experience, this is a tradeoff I was willing to take for the simplicity and efficiency of getting the commentting system set up.

# Implementation
I roughly followed the steps that Don Williamson laid out in his [tutorial](http://donw.io/post/github-comments/) plus all the suggestions commenters had left him.  The code below is mostly taken from his Github with slight modifications.  Make sure you change the `repo_name` variable if you use this code!

1. Create `static/js/github-comments.js`.
{{< gist 2e212e815369008ff5b65c98a5703312 >}}

2. Create `layouts/partials/comments.html`.
{{< gist 370ed0f35cec49de0d9d81eecf7d7b69 >}}

3. Copy over the default `layouts/post/single.html` from your theme. You might have to use `layouts/_default/single.html` instead if your theme does not provide a `layouts/post/single.html` file.
```
cp themes/after-dark/layouts/post/single.html layouts/post/single.html
```
Replace after-dark with your theme.


4.  Add this line to `layouts/_default/single.html` after the main content.
```
  {{ partial "comments.html" . }}
```

5. In each blog post, add `ghcommentid` as a field to the front matter after creating a github issue at the repository specified.  For example, this is what this post's front matter looks like.
```
+++
title = "Blog Comments"
date = 2017-12-03T09:40:40-08:00
description = "Adding comments using Github Issues"
draft = false
ghcommentid = 17
+++
```
Depending on your format of front matter, it could also look like:
```
---
title: "Blog Comments"
date: 2017-12-03T09:40:40-08:00
description: "Adding comments using Github Issues"
draft: false
ghcommentid: 17
---
```

# Conclusion
Using Github Issues was an efficient way to get a commenting system off the ground that maintained page speed, didn't require self-hosting, and didn't compromise reader privacy.

There are still some issues such as rate limiting by the Github API which prevents a single IP address from making more than 60 requests per hour.  I could implement authorization to bypass this but then the aggregate of users can't exceed 5000 requests per hour.  Right now, I hope to eventually add the authorization only if a user has already exceeded his or her 60 requests that hour but that still seems vulnerable to DDoS attacks.  To resolve that, I could cache it on a server somewhere and limit fetching from the Github API to every second.  Let me know in the comments section if you have any suggestions or if this is even necessary.  Will readers usually read more than a blog post a minute?
