+++
title = "Advent of Code Day 1"
date = 2017-12-01T07:13:53-08:00
description = "Lessons from my first attempt at the Advent of Code challenge"
draft = false
ghcommentid = 15
+++

[Advent of Code](https://adventofcode.com/2017) is an online coding competition that has been going on since 2015.  My co-workers participated last year to learn Elixir and other languages (Clojure, Haskell, etc.).

This is my first online coding competition.  My goal was to get at least 1 point which required me to be one of the first 100 finishers on one of the 25 days.

According to the [founder's Twitter page](https://twitter.com/ericwastl/status/936353398076723200), there are over 100,000 users so it will be no walk in the park.

# Setup
I used a single terminal window with a tmux pane with vim open to `1_1.py` and a tmux pane on the right to run my code. My strategy was to throw in extra print statements to check myself early so I wouldn't have to waste my time debugging. My initial plan was to finish part 1, and then write to `1_2.py` and modify that file for part 2.

Also, I planned to not worry as much about clean code as finishing since my goal was time.  The tradeoff could be that it could bite me in part 2 but I wanted to avoid premature optimization.

I hope to go back and do the problems in a new language (Elixir, Haskell, Clojure, Go, Nim) but I figured to optimize for points, I should go with Python because of dynamic typing and since it's a scripting language. My Python is pretty rusty but I figured throughout the competition, I would be able to ramp up.

# Part 1
```
--- Day 1: Inverse Captcha ---

The night before Christmas, one of Santa's Elves calls you in a panic. "The printer's broken! We can't print the Naughty or Nice List!" By the time you make it to sub-basement 17, there are only a few minutes until midnight. "We have a big problem," she says; "there must be almost fifty bugs in this system, but nothing else can print The List. Stand in this square, quick! There's no time to explain; if you can convince them to pay you in stars, you'll be able to--" She pulls a lever and the world goes blurry.

When your eyes can focus again, everything seems a lot more pixelated than before. She must have sent you inside the computer! You check the system clock: 25 milliseconds until midnight. With that much time, you should be able to collect all fifty stars by December 25th.

Collect stars by solving puzzles. Two puzzles will be made available on each day millisecond in the advent calendar; the second puzzle is unlocked when you complete the first. Each puzzle grants one star. Good luck!

You're standing in a room with "digitization quarantine" written in LEDs along one wall. The only door is locked, but it includes a small interface. "Restricted Area - Strictly No Digitized Users Allowed."

It goes on to explain that you may only leave by solving a captcha to prove you're not a human. Apparently, you only get one millisecond to solve the captcha: too fast for a normal human, but it feels like hours to you.

The captcha requires you to review a sequence of digits (your puzzle input) and find the sum of all digits that match the next digit in the list. The list is circular, so the digit after the last digit is the first digit in the list.

For example:

  - 1122 produces a sum of 3 (1 + 2) because the first digit (1) matches the second digit and the third digit (2) matches the fourth digit.
  - 1111 produces 4 because each digit (all 1) matches the next.
  - 1234 produces 0 because no digit matches the next.
  - 91212129 produces 9 because the only digit that matches the next one is the last digit, 9.

What is the solution to your captcha?

Your puzzle answer was 1069.
```

I lost some time reading the introduction before jumping into the problem.  I read up to the third paragraph before realizing that it was a long winded introduction and I could skip to the bottom.

Here was my solution:
```python
INPUT="36743676522426214741687639282183216978128565594112364817283598621384839756628424146779311928318383597235968644687665159591573413233616717112157752469191845757712928347624726438516211153946892241449523148419426259291788938621886334734497823163281389389853675932246734153563861233894952657625868415432316155487242813798425779743561987563734944962846865263722712768674838244444385768568489842989878163655771847362656153372265945464128668412439248966939398765446171855144544285463517258749813731314365947372548811434646381595273172982466142248474238762554858654679415418693478512641864168398722199638775667744977941183772494538685398862344164521446115925528534491788728448668455349588972443295391385389551783289417349823383324748411689198219329996666752251815562522759374542652969147696419669914534586732436912798519697722586795746371697338416716842214313393228587413399534716394984183943123375517819622837972796431166264646432893478557659387795573234889141897313158457637142238315327877493994933514112645586351127139429281675912366669475931711974332271368287413985682374943195886455927839573986464555141679291998645936683639162588375974549467767623463935561847869527383395278248952314792112113126231246742753119748113828843917812547224498319849947517745625844819175973986843636628414965664466582172419197227695368492433353199233558872319529626825788288176275546566474824257336863977574347328469153319428883748696399544974133392589823343773897313173336568883385364166336362398636684459886283964242249228938383219255513996468586953519638111599935229115228837559242752925943653623682985576323929415445443378189472782454958232341986626791182861644112974418239286486722654442144851173538756859647218768134572858331849543266169672745221391659363674921469481143686952478771714585793322926824623482923579986434741714167134346384551362664177865452895348948953472328966995731169672573555621939584872187999325322327893336736611929752613241935211664248961527687778371971259654541239471766714469122213793348414477789271187324629397292446879752673"

sum=0
prev = "3"
for char in INPUT:
    print char
    if char == prev:
        sum = sum + int(char)
    prev = char
print sum
```
If clean code was a factor, I should have used `INPUT[-1]` instead of hard coding the last value in prev.  Also the lack of spaces around the equals when setting sum really bothers me now but probably saved me time.  I had to look up how to convert a char to an int due to not using python for a while which slowed me down. I achieved my goal for Part 1, finishing at rank 86 with a time of 3 minutes, 32 seconds according to my personal leaderboard!

# Part 2
```
--- Part Two ---

You notice a progress bar that jumps to 50% completion. Apparently, the door isn't yet satisfied, but it did emit a star as encouragement. The instructions change:

Now, instead of considering the next digit, it wants you to consider the digit halfway around the circular list. That is, if your list contains 10 items, only include a digit in your sum if the digit 10/2 = 5 steps forward matches it. Fortunately, your list has an even number of elements.

For example:

  - 1212 produces 6: the list contains 4 items, and all four digits match the digit 2 items ahead.
  - 1221 produces 0, because every comparison is between a 1 and a 2.
  - 123425 produces 4, because both 2s match each other, but no other digit has a match.
  - 123123 produces 12.
  - 12131415 produces 4.

What is the solution to your new captcha?

Your puzzle answer was 1268.

Both parts of this puzzle are complete! They provide two gold stars: **
```

My heart now racing a little more, I realize that I just needed to change the `prev` variable so that it got the element halfway across the list.  I don't bother creating a new file `1_2.py` since it seems like such an easy fix.
```python
INPUT="36743676522426214741687639282183216978128565594112364817283598621384839756628424146779311928318383597235968644687665159591573413233616717112157752469191845757712928347624726438516211153946892241449523148419426259291788938621886334734497823163281389389853675932246734153563861233894952657625868415432316155487242813798425779743561987563734944962846865263722712768674838244444385768568489842989878163655771847362656153372265945464128668412439248966939398765446171855144544285463517258749813731314365947372548811434646381595273172982466142248474238762554858654679415418693478512641864168398722199638775667744977941183772494538685398862344164521446115925528534491788728448668455349588972443295391385389551783289417349823383324748411689198219329996666752251815562522759374542652969147696419669914534586732436912798519697722586795746371697338416716842214313393228587413399534716394984183943123375517819622837972796431166264646432893478557659387795573234889141897313158457637142238315327877493994933514112645586351127139429281675912366669475931711974332271368287413985682374943195886455927839573986464555141679291998645936683639162588375974549467767623463935561847869527383395278248952314792112113126231246742753119748113828843917812547224498319849947517745625844819175973986843636628414965664466582172419197227695368492433353199233558872319529626825788288176275546566474824257336863977574347328469153319428883748696399544974133392589823343773897313173336568883385364166336362398636684459886283964242249228938383219255513996468586953519638111599935229115228837559242752925943653623682985576323929415445443378189472782454958232341986626791182861644112974418239286486722654442144851173538756859647218768134572858331849543266169672745221391659363674921469481143686952478771714585793322926824623482923579986434741714167134346384551362664177865452895348948953472328966995731169672573555621939584872187999325322327893336736611929752613241935211664248961527687778371971259654541239471766714469122213793348414477789271187324629397292446879752673"

sum=0
prev = "3"
print len(INPUT)
for i in xrange(0, len(INPUT)):
    char = INPUT[i]
    prev = INPUT[(i + len(INPUT)/2) % len(INPUT)]
    if char == prev:
        sum = sum + int(char)
print sum
```
I jump a few ranks and finish Part 2 as number 79 with a total time of 5 minutes, 32 seconds meaning that it took me exactly 2 minutes to solve part 2!


# Conclusion
After Day 1, I have 37 more points than I expected and I collected my gold stars.  On the [leaderboard](https://adventofcode.com/2017/leaderboard/day/1) I'm tied for 87 which means that several people who finished Part 1 after me, must have finished both parts before I did.  It's still exciting and a pleasant surprise to see myself on the leaderboard!  I'm praying that I can keep it up for a bit longer!  So much fun in so little time!!!

On the personal leaderboard statistics page, I get to see these fun stats:
```
These are your personal leaderboard statistics. Rank is your position on that leaderboard: 1 means you were the first person to get that star, 2 means the second, 100 means the 100th, etc. Score is the number of points you got for that rank: 100 for 1st, 99 for 2nd, ..., 1 for 100th, and 0 otherwise.

You have 37 points.

      -------Part 1--------   -------Part 2--------
Day       Time  Rank  Score       Time  Rank  Score
  1   00:03:32    86     15   00:05:32    79     22
```
