+++
title = "Common Sense on Mutual Funds"
date = 2018-02-25T20:14:17-08:00
description = "Notes on the fully updated 10th edition of Bogle's Common Sense on Mutual Funds"
draft = false
ghcommentid = 22
+++

# On Investment Stategy
[Link to book](https://amzn.to/2tucGnD)
## On Long-Term Investing
Bogle refers to Wharton's Professor Siegel's research from Stocks for the Long Run on US stock and bond returns between 1802 and the present (2010).

"Stocks grew at a real rate of 7 percent annually; bonds, at a rate of 3.5 percent." (10)

Stock returns were actually around 7 percent for all three major periods in US history(from industrialization, 1802-1870, globalization, 1872-1925, and the modern stock market, 1926-1999). In comparison, bond returns were variable (4.8, 4.8, and 2.0 percent). Stock returns actually dropped to 6.5 annually when observed up to 2009 (but possibly has bounced back in our latest bull run). Bonds jumped from 3.5 percent through 1997 to 6.2 percent for the next decade (12).

Since 1980, gold has lost 40 percent of its real value. (12)

Inflation has been high in the modern era (0.1 and 0.6 percent the two previous eras) due to coming off the gold standard (14).

Bonds have outperformed stocks in 8 out of 172 (1 out of every 21) rolling 25-year periods since 1802 (24).
Perhaps I should rethink my 0% bond strategy.  I have considered a 90/10% stock/bond allocation before which Vanguard points out has outperformed 100% stocks but in the spirit of simplicity opted to stick with 100% bonds.

  1. "All investors own the entire stock market, so both active investors (as a group) and passive investors--holding all stocks at all times--must match the gross return of the stock market."

  2. "The management fees and transaction costs incurred by active investors in the aggregate are substantially higher than those incurred by passive investors."

  3. "Therefore, because active and passive investments together must, by definition, earn equal gross returns, passive investors must earn the higher net return. QED."
(QED is what mathematicians use to denote the end of a proof.) (26)

My Caltech BEM 107 professor, Professor Bradford Cornell, pointed that some active investors argue they were in the half of the active investors that outperform the market but rarely does this outperformance persist over time and beat the fees charged.

Market timing usually hurts investors since they jump out only to lock in losses and jump back in at a higher price due to their fear of missing out.  Active managers as a group have consistently followed this trend.  In addition, stock turnover today is historically high which means there is proportionally more money trying to time the market (and failing).  Since the book has been published, the inflow of high frequency trading firms and quantitative firms has probably pushed the turnover rate even higher.

Jack's Simple Principles for Long-Term Success (41-42)

1. "Invest you must."

2. "Time is your friend."

3. "Impulse is your enemy."

4. "Basic arithmetic works."

5. "Stick to simplicity."

6. "Stay the course."

## On the Nature of Returns - Occam's Razor
Who knew that IB Philosophy class would finally be useful? (Russell's teapot and Occam's Razor)

Over the long run, stock returns will reflect their fundamental returns. In the short run, speculation drives prices up and down.

"So, courtesy of Occam's Razor, I advance a simple theory.  These three variables determine stock market returns over the long term:

1. The dividend yield at the time of the investment.

2. The subsequent rate of growth in earnings.

3. The change in the price-earnings ratio during the period of investments." (51)

Many companies today (especially tech companies) tend to eschew paying out a dividend and opting instead to reinvest earnings into the company.  This shields shareholders from paying taxes on the dividends and has been used as an argument for why the dividend yield has fallen.  Perhaps Bogle's omission of this argument indicate that this is not actually causing earnings growth to increase.

Even on the decade level, taking the sum of these 3 factors has been within 0.7% of the actual returns. Speculation (the third factor) can overshadow investment changes (first and second factors) for decades.

Even Bogle can't predict the future return of the stock market using reasonable estimates for dividend yield and earnings growth (1991-1999) although he can point out why his estimates may be wrong (speculation). He even admits that when he is right, it is luck.

Bond returns are driven by (mostly) initial interest rates, reinvestment rate, and terminal yield.

"Occam's Razor will not tell us what future returns will be, but it will tell us what the elements of stock and bond returns must be to provide us with any rate of return we wish to assume" (74).

"So, invest with intelligence and common sense; engage in an enlightened and rational discourse when considering the future; always have some significant portion of your assets both in stock and in bonds; be sparing a about precipitate and extreme changes in these proportions.  And be skeptical about every prognostication you are given, including mine" (75).

Jack really doesn't want to answer my question of whether or not I should hold bonds...

## On Asset Allocation
Bogle's recommended starting point is two thirds stocks, one third bonds. 80/20 for a younger investor in the accumulation phase to 50/50 for an older investor in the distribution phase (70/30 for older accumulator, 60/40 for younger person taking distributions).

"It would not be imprudent for a highly risk-tolerant young investor (25 years old  or so), who is just beginning to invest for retirement, to allocate everything to stocks, provided that the investor has confidence that regular investments could be made through thick and thin" (85)

Surprisingly Bogle advocates three strategies - fixed ratio by rebalancing, setting an initial allocation and letting it ride, or tactical asset allocation where 15 percentage points can be shifted.  He emphasizes this may only work if you have skill, insight, and luck.

Over 90% of the variation in total penion plan returns was explained by allocation of assets.  (The rest was market timing and security selection.)  However, costs are even more important.

In a study of pension fund and mutual fund managers, even before deducting costs, the higher cost managers had lower returns (when averaged and split into quartiles) probably because of their portfolio transaction costs.

Even before deducting operating costs, higher cost managers trail lower cost ones! So if you want to pick out the losers, this is one easy way to tell them apart.

I was browsing through a copy of Forbes magazine at the orthodontist the other day and there was a quote from a manager saying that she believed having costs as a percentage of portfolio value tied the firm's interests with the customer's interests.  (Instead this probably just motivates them to get more clients to increase their assets under management.)  I couldn't help but wonder with the mountain of research against these active funds, whether they knew they were lying or whether these managers have convinced themselves of this lie to help them sleep at night.

The equity risk premium is the extra return of the stock market over risk-free US Treasury bonds.  The historical average is 3.5 percent.  One way to think of costs is as a percentage of the risk premium.

By cutting costs, investors can achieve higher returns with less risk (because they could move to a lower stock allocation and still come out ahead).

Bogle originally wrote this chapter considering only the 1.5 percent average expense ratio.  However, there is also an additional 0.5-0.8 percent lost to portfolio turnover and even more if there is a sales load.

## On Simplicity
The simplest approach is owning one balanced market index fund with stocks and bonds which has captured 98% of the market return.

Bogle has 8 rules if you decide not to index.
Select low-cost funds.
Consider carefully the added costs of advice.  (In particular, 12b-1 fees which are hidden and wrap accounts which have an additional fee on top of the fees of the mutual funds they package.)
Do not overrate past fund performance.
Use past performance to determine consistency and risk.
Beware of stars.
Beware of asset size.
Don't own too many funds.
Buy your fund porfolio and hold it.

"Forecasts about fund return can be made:

1. Funds with unusually high expenses are likely to underperform appropriate market indexes.

2. Funds with past relative returns that have been substantially superior to the returns of an appropriate market index with regress toward, and usually below, the market mean over time."
The Legg Mason Value Trust beat the S&P500 for 15 years only to lose 66% from 2006-2008 (when the S&P500 lost 23 percent).

A single index fund provides as low of a risk as a 30 mutual fund portfolio. (134)

The most noteworthy comment in the chapter was that Bogle does not recommend any more than 20% international exposure due to economic and currency risks.  He prefers to emphasize emerging markets.

# On Investing Choices
## On Indexing
The Wilshire 5000 is actually the entire US stock market (it had 7400 stocks when the book was originally published but at the end of 2017 was at 3492).

The S&P500 dominates the majority of active managers except for the few years when small and mid cap stocks were super hot.  This is expected because the S&P500 is not the market (Wilshire 5000) but in the long run it doesn't matter since their rates of return are essentially the same.  The market is 75 percent large-cap, 15 percent mid-cap, and 10 percent small-cap by value.

Bogle did not invent the idea of the index fund but Vanguard was the first company to implement it successfully.

When 15 year gross returns (net return + expenses + 50 basis points) of actively managed funds ending 1998 were compared to the Wilshire 5000, they were approximately a normal distribution which suggests that returns were just based on luck.  The distribution is slightly skewed to the right for 25 year gross returns ending 2008 but costs still end up moving the distribution to skew left.

Efficient index funds trail the market by less than 0.2 percent which is 1.7 percent better than the average mutual fund.  This 1.7% plus sales charge (0.5%), survivorship bias (1%), and tax efficiency (1%) means that an efficient index fund would have a 4.2% advantage.  The counterargument is that actively managed funds are less risky but in the 1990s, an index fund was 15% less risky.

Not all index funds are created equal.  Vanguard funds are large, have no sales load, have rock bottom costs, low turnover, and are managed efficiently.

Not all index fund managers are created equal as some trail the index more than others.  Examine a manager's implementation of strategy, unrealized capital gains (more is worse), and tax management.

An argument commonly made against indexing is that it would only work in efficient markets since inefficient markets allow more arbitrage opportunities.  However, indexing works in any market because the average (the index) beats the below average funds and then even some of the above average funds once costs are added in.  In fact, costs in inefficient markets are usually higher.  For example mid-cap and small-cap underperform more than large-cap compared to their indexes.

Another example are international funds which have higher custodial costs, taxes, commissions, and market impact costs.

Since bond funds have lower expected returns, costs make a bigger impact by cutting a greater percentage of the return.  In addition, there are no heroes (no "Peter Lynch's" in bonds) because there is less volatility and thus it is harder to outperform by a significant margin. (I found it interesting that Bogle references Lynch's book Beating the Market since I had read it near the beginning of my investing journey.)

Some ETFs are good if you buy and hold them but the problem is that they are not always broad market indexes and have huge turnovers which means they encourage short term speculating.

## On Equity Styles
Bogle uses the Sharpe ratio to compare styles in up markets and Modigliani formula to compare in down markets (since it doesn't make sense to use Sharpe to compare negative returns).  The Modigliani formula adjusts annual returns relative to the risk vs the market.  Indexing in every style wins either due to increased returns or similar returns and lower risks.

Choosing indexing versus choosing an actively managed fund is like Pascal's wager.

At the end of the day, Bogle advises against playing the style game since it is a loser's game but if you must, use index funds.

Since he often recommends the S&P500, I found it of note that he said the total market is more conservative.  "Rather than emphasizing particular styles, however, a simpler course would be for you to index your entire equity portfolio with the Standard & Poor's 500 Stock Index. A more conservative-and more certain-wager would be to index your portfoilio to the total stock market" (212).

His main reason for including this chapter was to prove the naysayers who claim the S&P500 outperformed mutual funds since it had more large cap wrong.  Indexes outperform in all styles.

## On Bonds
Bonds are commodity-like securities (means individual securities followed similar rates of returns) in a highly efficient market.  Thus, it is difficult to get a big margin before costs.

Lynch sees no purpose in bond funds over individual bonds but Bogle points out that the advantage is that the maturity is fixed over time since the index will keep buying new bonds.

Low cost bonds outperform and have lower risk based on duration, volatility, and portfolio quality (based on S&P's ratings).

Long-term municipal bonds, short-term US-government bond funds, intermediate US government bond funds and GNMA funds all show a trend where the funds had over a percentage of additional return for every percent of cost.

Intermediate term corporate bond funds have closer returns (but still inversely related to costs) but the higher cost funds had much more risk.

The story is the same for bond funds: high cost funds are worse than low cost funds and both are beat by index funds.

## On Global Investing
Bogle reiterates that he would put a maximum of 20 percent in international stocks but thinks 0 percent is best.

The argument is that a global portfolio has less volatility, more stability due to the diverse economic base, higher potential growth, and cheaper valuations (253).  For example emerging markets killed it from 1998-2008.  The caveat is that currency risk can lead to the illusion of higher returns if the dollar is weaker.

Currency risk is a short term risk and long term, international returns will be based on fundamental returns (dividend yields plus earnings growth).  He points out the issue with all the efficient frontier arguments are that they are based on the past.

He points out that market risk is mandatory.  Some people also choose to take on style risk and manager risk.  Currency risk is another optional risk that we are choosing to take on when we invest internationally.

In 1990, Japan was 43% of the world market capitalization. As of 2017, it still is below its 1991 highs (although it has risen above them periodically). Countries will revert to global means eventually but don't try to time the market.

There is additional risk in emerging markets since these markets do not have US standards for accounting, financial transparency, and have lower liquidity. From 1960-1997, the S&P500 and the EAFE international index had nearly identical returns.

The average operating cost is higher and turnover is lower (although still high at 70%) for international funds.  International funds have higher liquidity costs, transaction costs, trading costs, stamp taxes, and custodial fees.  The average international mutual funds has around 4% active costs versus a less than 1 percent cost for an international index funds (operating expenses and transaction costs).  This is a greater advantage (1.5 times) than U.S. index funds have over actively managed US funds.

When examining the data, costs are still a real obstacle but we have incomplete data about all investors.  U.S. managers may still win or lose in the short run but will probably perform around the same in the long run (under the assumption they aren't dumber than their international counterparts). In funds that invest in European stocks, index funds had a 3.4% advantage.  In Pacific funds (only 3), surprisingly active funds beat the index 4.4% to -1.2% but that might be biased by small sample size.

"Some international funds can and do defy the odds that so heavily favor index funds. In general, they are funds that tend to have these characteristics: highly experienced managers in place for an extended period, relatively low portfolio turnover, and modest operating costs and advisory fees... Nonetheless, times change, managers are replaced, policies are revised, and expense ratios often more upward.  For the long-term investor interesting in spreading investments around the globe via mutual funds, international index funds offer a sensible approach" (271-272).

S&P500 has 28% of its net income (23% aggregate revenue) coming from outside of the U.S.  Bogle points out that we are on top of the world at the end of the 1990s and could fall like Japan.

10 years later after the initial publication, Bogle doubles down that no more than 20% of the equity portion of our portfolio should be international.

## On Selecting Superior Funds
Funds will revert towards the mean.

During  the 80s-90s bull run, expense ratios and turnover reached their highest points and as a result, active funds only captured 87% of the market return (which is terrible when compounded).

Many index funds exist nowadays due to 401k competition.  Watch out for the illusion of low fees due to temporary fee waivers, high expense ratios, and sales load or 12b-1 fees.

"Index funds are threatening to become the holy grail of mutual fund investing" (282).  Now in 2017, they are the holy grail of investing.

Bogle asks whether choosing funds based on past performance can outperform the market.

1. The Sharpe study indicated that choosing the top 25 of other past year of the 100 largest funds would have generated an additional 0.8 percent relative to the index (which quickly evaporates after taxes).

2. The Carhart study showed that survivorship bias adds 1.4% to active fund returns.  There is also some persistence since 17% of the top decile repeats (momentum effect?) and 46% of the bottom decile repeats (might be due to high costs).

3. The Goetzmann-Ibbotson study showed that even the winners among the active funds might still lose to index.

Risk is persistent but not returns.

From 1982-1999, 258 funds survived and 126 did not.  By, 2008, an additional 77 of those original funds had disappeared.

Funds of funds suck and have an additional layer of costs on top of the individual fund costs.

## On Reversion to the Mean
Superior returns revert to the mean. Very large funds rarely rise again after reverting. Many mutual fund managers ignore taxes.

"In the run, a well-diversified equity portfolio is a commodity, providing rates of return that are highly likely to resemble closely and finally fall short of those of the stock market as a whole" (306).  This means that in the long run, the gross return of equity portfolios approximates the total market return.

"Reversion toward the market mean is the dominant factor in long-term mutual fund returns" (307)

No study suggests that long-term winners can be selected in advance.

Reversion to the mean is especially evident in the transition from bull to bear market. My guess is because excess returns were associated with the amount of risk taken.

Growth stocks and value stocks returns 11.7% and 11.5% over 60 years and each had periods of outperformance before reverting to the mean and underperforming (sometimes decades so don't try timing it).

High-grade vs low-prices stocks also exhibit reversion to the mean.

Large cap versus small cap have returned 13% vs 10.7% but also have exhibited reversion to the mean. While many Bogleheads advocate overweighting small cap stocks for the excess reason, Jack reminds us that "investors who hold small-cap stocks disproportionately larger than their market weight would be well-advised to have a full measure of patience" (319).  He also points out that without the dominant 1973-1983 decade for small cap stocks, large cap beat small cap 11.1% to 10.4% (in 1999).  In the addendum of the 2008 edition, Bogle admits that "the small-cap advantage over large-caps is substantial in terms of historical annual return" (319).

US and international stocks have remarkably similar long term returns of 9.1% vs 9.0%. (Perhaps that is why Mr. Bogle is unwilling to take on additional currency risk for the same returns.)

Even common stocks exhibit reversion to the mean.  Some argued that stocks are reverting to a higher mean in 1999.  The following decade proved that this was not the case.  This is a good reminder since in 2018, the P/E is high compared to the historical mean and returns have been high.  In spite of the high valuations of the market, forecasting is a hazardous and there are powerful odds against being right twice.  So these high valuations should cause us to evaluate our investing goals rather than to try to forecast the market.

Bogle recommends 70/30 stocks and bonds as a starting point, up to 90/10 if the investor has a stomach for risks and long time horizon (15-40 years).

Bogle also states that the total US index is the best response for reversion to the mean.  (This was surprising for me since he seems to love the S&P500.)  He does say that in the long run, the S&P500 is reasonable too.

Using low cost investing and tax deferred plans are the "crown jewels" for the long term investor.  "An investment program that carries the theoretical armor of RTM, the mathematical armor of regular investing, and the protective armor of a balanced strategy, combined with the powerful weaponry of compound interest, deferred taxes, and low cost, would be applauded by Sir Isaac Newton" (328).

## On Investment Relativism
Funds report time-weighted return (which they can game by throwing in a bunch of money into well performing funds at the end of the year) instead of dollar weighted returns (which is more accurate).  This boosts an industry average 6.5% true return to a stated 9.8% return.

Relativism (by short term comparison to the S&P500 or total market) is actually hurting active managers because they end up just imitating the market by holding the largest stocks for insurance.

Because of the prevalance of quantitative measurements such as beta, alpha, and Sharpe ratio, managers seek to avoid inferior short term returns instead of achieving superior long term returns (335).  This is why the chances of another Peter Lynch or Warren Buffett is declining.

These "closet index funds" (which is a bad thing since it guarantees underperformance) can be identified by large asset size, portfolio composition of 80% large-cap stocks, more than 15 of 25 of their largest holdings is among the 25 largest stocks, or a correlation R^2 statistic of 0.95 or more.

In the original edition of the book, Bogle suggested that some quantitative funds can do well since a computer algorithm should be cheaper and thus low-cost.  However, Bogle admits that he was wrong when in 1999=2008, quants attracted a lot of money but their returns were negative relative to the index.  As Benjamin Graham said, "in the stock market the more elaborate and abstruse the mathematics the more uncertain and speculative are the conclusions we draw therefrom" (344).

## On Asset Size
Mutual funds hold over 21% of US stocks in 1999 which encourages a herd mentality as the industry gets too big and continues turning over so many stocks.  Index funds don't have to worry because size doesn't affect them since they have low turnover and no active management. Performance deteriorates for active funds as they grow in size.

The fund sizes advertised are usually not accurate because the company usually has many other funds which may have similar holdings.

Large size hurts performance because:
1. There are fewer stocks to choose from since managers don't want to hold more than 10% of the market cap of a company.

2. There are higher transaction costs since buying and selling moves the market more.

3. There are more processes and committees which means there can't be a few bright managers.

Bogle also gives some insight into why he was so positive about quants since his son, John C. Bogle Jr. was a quantitative trader (the only manager who discloses trading costs publicly).

## On Taxes
To handle growing asset size, Bogle suggets:

1. Change fund's strategy to a long term, low turnover approach but don't change the objective of achieving better than market returns.

2. Close funds to new investors.

3. Let funds grow but add new managers. - Have a new manager handle a portion of the portfolio to minimize the impact of size on transaction costs.

4. Lower basic advisory fee, add incentive fee (with penalty) for beating the market. - Most hedge funds have an incentive for positive returns but no penalty for negative returns. In addition, Bogle sets the baseline at the market return rather than 0.

5. Size proof mutual funds through minimum turnover and a nominal fee.

The unrealized ta liability that many funds carry in bull markets is leveraged.  (With 25% gains, a 25% market increase could double the taxes whereas a 25% drop would elimate the gains and the taxes.) Deferred capital gains tax is an interest-free loan.  Consider unrealized capital gains in a portfolio of funds when buying into it. (This is one case where good past performance hurts future returns.)

Expenses represent a 75% tax on gross income before taxes.  Equity funds average -1.9% alpha (which compares performance to the market) and -5.1% alpha after taxes.

The 90% turnover in 1999 was a big jump from 30% in 1974 and is even higher today.  This is worse for taxes because 30% of capital gains are realized in the short-term.  Limited turnover defers taxes and heirs would receive shares with a new cost bases to eliminate taxes altogether!  Average taxed managed fund has a 10.2% return versus the average of 8.0% (although both these returns are overstated due to the methodology used).

In 2003, long term capital gains tax went from 20 to 15% (thank you Uncle Bush).  Index funds are tax efficient because composition rarely changes significantly.

Vanguard goes from the 94th percentile of funds to 97th percentile after taxes.  Index funds build up unrealized gains and could be forced to realize them if enough shareholders sell.

Bogle suggests that a tax managed fund should be a market index fund emphasizing growth and low yield equities with tax loss harvesting (which some funds do employ today) and a penalty if shares are redeemed within 5 years (which I haven't heard of being implemented yet), and rock bottom costs. The issue with most tax managed funds is no short term penalty and high costs.

Bogle's idea (the Nifty Fifty) is to start a fund to buy the 50 largest stocks (so no manager is required), charge stiff redemption fees and limit liquidity, and to pay investors in shares instead of selling when investors want out.  Based on the 25 years since 1971, this strategy would have outperformed the market (although I wouldn't put too much weight into these results). The Founders Mutual Fund which held the same 36 stocks for 45 years underperformed the S&P500 in part due to it's operating cost sbut beat the Massachuseets Investors Trust after taxes although both had the same returns.

Through 2008, tax managed funds didn't do well because the market had no gains so there was nothing to tax.

Investors churning ETFs make a normally tax efficient fund inefficient due to the short term gains.

## On Time
"Never make an investment without having a clear idea of the impact each dimension [reward, risk, cost, time] will have" (401).  How many long-term investors can you name besides Warren Buffett?  Time shapes perception of risk and return.  Although we often talk about the power of compound interest, there is also the tyranny of compounding, which is the impact of compounded costs over time.

# On Fund Management
To be continued...
