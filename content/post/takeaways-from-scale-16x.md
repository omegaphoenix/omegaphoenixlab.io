+++
title = "Takeaways From SCaLE 16x"
date = 2018-03-11T23:29:49-07:00
description = "3 Days at SCaLE 16x - Resources and Experience"
draft = false
ghcommentid = 23
+++

# Friday
This was my first conference and it was an amazing experience!

## Continuous Integration and Deployment with Gitlab or Jenkins - Aleksey Tsalolikhin
My first conference event was a workshop on hosting your own Gitlab server and setting up CI and CD with a Gitlab runner.

We explored using Gitlab to build, test, and deploy to both a staging and production environment.

As soon as I heard there were prizes for being the first to finish, I dived in and leveraged my prior experience setting up a Gitlab runner to earn... a small bag of Sour Patch kids.

The slides, which contains the full tutorial, are on [GitPitch](https://gitpitch.com/atsaloli/cicd/master?grs=gitlab#/)  It seems like the slides may be down after the presentation.  I downloaded them during the presentation so contact me and I'll be happy to share the tutorial.

## Tuning PostgreSQL for High Write Workloads - Grant McAlister
This was essentially a sales pitch for AWS Aurora but it was a very interesting exploration of the limitations of Postgres in high-write systems and how Aurora solves those problems. The speaker suggested watching his AWS re:Invent talk on Aurora.

## The Many Faces of PostgreSQL Replication - Phil Vacca
Postgres' write-ahead log (WAL) makes the database durable. Streaming, synchronous streaming, and logical replications (by bytes) were the three main replication types covered.

Look up Caltech's CS121 and 122 classes by Donnie Pinkston to get amazing and free lecture recordings on databases.

## Expo
The expo is the main event of the conference. It was similar to a career fair with a much friendlier vibe. At first I was going to skip the Expo to go to as many talks as possible, but thanks to my co-workers and the SCaLE 101 talk, I came to realize that the main purpose of the conference was to interact with human beings even though that is hard for the typically introverted engineer.  Next year I will definitely spend more time in the Expo hall and work on cool things like soldering and building the DC Darknet badge!

## Managing CoreOS and Kubernetes with Puppet - Lucy Wyman
This talk went a bit over my head since I was familiar with the topics covered.  Puppet manages user and file permissions for machines. CoreOS has a Container Linux operating system which is a slimmed down Linux container with docker. Kubernetes schedules resources for applications running in the container. Containers and CoreOS are "immutable," so Puppet is used to manage the configuration files for CoreOS.

Slides [here](http://slides.lucywyman.me/) if you are interested.

## DevOps After Dark -  Rami Al-Ghanmi, Mosab Al-Ghanmi
The biggest takeaway from this talk was using [Yet Another Dotfile Manager](https://thelocehiliosan.github.io/yadm/) to manage `vim`, `tmux`, `ssh`, `gpg`, and `zsh` for different operating systems.  It can even install vim plugins and track confidential files by encrypting them in version control. [Here](https://github.com/alghanmi/dotfiles) is his example.

He also had a [cool converter](https://github.com/alghanmi/PodcastFeedGenerator) for library audiobooks to podcasts and suggested using `gpg` instead of `ssh` and 2FA with Yubikey.

He uses the Ubiquiti EdgeRouter X and a network topology which separates his IoT and personal devices.


# Saturday
## Microsoft's Open Source Software Evolution - John Gossman
John talked about how Microsoft went from a company whose CEO (Steve Ballmer) said Linux was a cancer to a company which fully embraced open source and partnered with companies supporting open source.  He promised he wouldn't sell anything but couldn't contain his excitement for VSCode (which was even the most popular editor/IDE for Golang programmers) and Typescript which are both free and open-source (I use both every day!).  All of networking traffic in all of Microsoft's datacenter running through [SONiC](http://azure.github.io/SONiC/) which is opensourced.

## SCALE 101 - Phil Dibowitz
This talk was based off the Def Con 101 talk for noobs. The most important part of the talk was to remember that talking to humans was the most important part of SCaLE (a good reminder for me since I just wanted to go to as many talks as possible). Phil also encouraged us to step out of our comfort zone and go to new talks.  This advice paid dividends later on when I decided to step out of my comfort zone and check out CTF and #badgelife.

## You think you're not a target? A tale of three developers... - Chris Lamb
Chris used 3 examples of malware being introduced in executables by blackmail, compromised computers, and compromised servers. The solution is [reproducible builds](reproducible-builds.org) which saves time, money, and the environment among a ton of other advantages.

## A Cloud in Every Home: Host servers at home with 0 sysadmin skills - Nolan Leake
Nolan described how he hosted his own email servers at home so the government can't steal your data from datacenters without your knowledge. He uses ZeroTier for protection to build an encrypted overlay network to protect from IoT devices that could compromise his network. 'Free' email hosting means that your data will be monetized.

## Capture the Flag
As I was wandering through the conference center to my next talk, I saw people competing in Capture the Flag which is a quiz and puzzle type competition.  I tried competing in the intermediate track and it was prety interesting although I started late and left midway through since the server was slow to respond and it was hard to keep up with the top teams going solo.  I ended up in 21st out of 29th place but learned a few things.  I would definitely want to revisit good Capture the Flag competitions and the CTF talk given on Sunday to learn more about security.


# Sunday
It seemed like daylight savings time knocked out the majority of the conference attendees from the previous day since the Keynote had around a fifth of the attendance as the previous day.

## Travel, Tribes and Contribution Tales - Zaheda Bhorat
Zaheda talked about her open source journey with OpenOffice and lessons she learned from the community.  The takeaway was to think about your superpowers and how you can use them to contribute to the open source community.

## The Hardware Demoscene of #badgelife - Brian Benchoff
Brian talked about the Def Con badges and how he designed, prototyped, and built a Mr. Robot badge.

## DarkNet Badge Hardware and Software - Gater Byte, Edward Abrams, Demetrius Comes
This was my favorite talk since it introduced me to the DC Darknet community.  I will be listening to and reading `Daemon` and `Freedom` by Daniel Suarez.  This community is doing for hackers what MakerSpace did for makers.  There is more information at [holon.network](holon.network) and [dcdark.net](dcdark.net).

## Crypto: Math Theory for the Practically Minded - Elijah C. M. Voigt
I came into this talk late since it was in the same timeslot as the DarkNet talk which finished early.  He motivated different use cases for cryptography and explained how to encrypt functions and inputs and outputs for example using garbled circuits. [Slides](https://slides.elijahcaine.me/crypto/#1)

# The Misses
## DC Darknet Badge
My biggest regret was not building the DC Darknet Badge at SCaLE so I could pair with other hackers at the conference and start solving the puzzles.  On the bright side, now I get to buy my own temperature controlled soldering kit.
## PGP Keysigning Party
I didn't know what a PGP key was but next year I will definitely be joining the keysigning party.  I also heard from the SCaLE 101 presentation, that they gave away free Yubikeys!
## Talks
I will update this section as I watch these talks on YouTube.
### Work Life Balance - A therapy session - Todd Lewis
I sat down for this talk but after reading the description, decided to go home and actually spend time with family instead of listening to a speaker tell me to spend more time with family.

I listened to the talk afterwards and there were good book recommendations.  He recommended researching dopamine addiction and another speaker reminded us to apply the plane warnings to put on our oxygen mask before we help others.
### UpSCaLE
### Git like a Pro - Alan Ott
### Cross-platform Mobile application development with Flutter (Dart) - Randal Schwartz, Wm Leler
### Practical AWK - AWKwardly Dealing with Data - Lan Dang
### Security Through Immutability at Netflix - William Bengtson
### Regular Expressions: A guided tour - der hans
### Kubernetes: A complete app deployment with encryption - Michael Petersen
### Keeping Your Ubuntu Systems Secure - Elizabeth K. Joseph
### Securing Your Data on PostgreSQL - Payal Singh
### Car Hacking Village Badge - Robert Leale
### Gamify Security Training with Developer CTFs - Kyle Rankin
