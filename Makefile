watch-draft:
	hugo server -D

ci:
	gitlab-runner exec docker pages

webcoach:
	webcoach https://www.jkleong.com

renew:
	sh renew-ssl.sh
